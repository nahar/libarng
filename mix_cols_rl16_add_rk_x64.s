# --------------------------------------------------------------------
#
# mix_cols_rl16_add_rk_x64.asm
#
# @version 1.0 (June 2007)
#
# This file contains the implementation of the mixColumnsRL16AddRk
# method in x64 assembler. This method does the mixColumn step of
# the AES in the bitslice domain, with a rotation of the result
# to the left by 16 positions followed by the AddRoundKey step.
# The two steps are merged for performance reasons.
#
# @author Robert Könighofer <robert.koenighofer@student.tugraz.at>
#
# This code is hereby placed in the public domain.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --------------------------------------------------------------------

.global mixColumnsRL16AddRk #MISMATCH: "PUBLIC  mixColumnsRL16AddRk"

.text     #MISMATCH: "_TEXT   SEGMENT"

# --------------------------------------------------------------------
#
# This method implements the MixColumns step with an additinal rotation
# to the left by 16 positions and the AddRoundKey step
# of the AES in a bitsliced way. These two operations are put together
# for performance reasons. The argument passed in RCX is the round key to be
# used for the key addition. It has to be in bitslice representation
# as produced by the initKey method. The method updates the 
# state, which has to be in bitslice representation in the registers
# R8 to R15 as well. The additional rotation to the left by 16
# positions is done to undo the rotation to the right by 16 positions
# done by shiftRowsRR16. The ShiftRows step can be done faster with
# an rotation to the right, and for the MixColumns step, any rotation
# by a multiple of 16 does not make any difference in the performance.

mixColumnsRL16AddRk: #MISMATCH: "mixColumnsRL16AddRk     PROC"

# The following equations calculate the MixColumns step where the
# result is rotated to the left by 16 positions (also 
# see the documentation):
#
#  bit[0] = bit[0] ^ 
#           rotateLeft(bit[7],16) ^ 
#           rotateLeft(bit[0]^bit[7],32) ^ 
#           rotateLeft(bit[0],48);
#  bit[1] = bit[1] ^ 
#           rotateLeft(bit[0]^bit[7],16) ^  
#           rotateLeft(bit[0] ^bit[7]^bit[1],32) ^ 
#           rotateLeft(bit[1],48);
#  bit[2] = bit[2] ^ 
#           rotateLeft(bit[1],16) ^ 
#           rotateLeft(bit[1]^bit[2],32) ^ 
#           rotateLeft(bit[2],32);
#  bit[3] = bit[3] ^ 
#           rotateLeft(bit[2]^bit[7],16) ^ 
#           rotateLeft(bit[2] ^bit[7]^bit[3],32) ^ 
#           rotateLeft(bit[3],48);
#  bit[4] = bit[4] ^ 
#           rotateLeft(bit[3]^bit[7],16) ^ 
#           rotateLeft(bit[3]^bit[7]^bit[4],32) ^ 
#           rotateLeft(bit[4],48);
#  bit[5] = bit[5] ^ 
#           rotateLeft(bit[4],16) ^ 
#           rotateLeft(bit[4]^bit[5],32) ^ 
#           rotateLeft(bit[5],48);
#  bit[6] = bit[6] ^ 
#           rotateLeft(bit[5],16) ^ 
#           rotateLeft(bit[5]^bit[6],32) ^ 
#           rotateLeft(bit[6],48);
#  bit[7] = bit[7] ^ 
#           rotateLeft(bit[6],16) ^ 
#           rotateLeft(bit[6]^bit[7],32) ^ 
#           rotateLeft(bit[7],48);

# to improve the speed, we make some precalculations of values,
# which are used very often:
# t0 = bit[0] ^ rotateLeft(bit[0],16) -> RAX
# t1 = rotateLeft(bit[1],16) ^ rotateLeft(bit[1],32) -> RBX
# t2 = bit[2] ^ rotateLeft(bit[2],16) -> RBP
# t3 = rotateLeft(bit[3],16) ^ rotateLeft(bit[3],32) -> RDX
# t4 = bit[4] ^ rotateLeft(bit[4],16) -> RDI
# t5 = rotateLeft(bit[5],16) ^ rotateLeft(bit[5],32) -> RSI
# t6 = bit[6] ^ rotateLeft(bit[6],16) -> RBX
# t7 = rotateLeft(bit[7],16) ^ rotateLeft(bit[7],32) -> RAX
#
# This leads to a much faster solution than just precomputing
# bit[0]^bit[7] and bit[3]^bit[7]:
# 10 MOV         8  MOV
# 24 ROL   =>   20 ROL
# 34 XOR        27 XOR
#
# The temporary variables t0 to t7 correspond to the 
# following registers:
# t0 = RAX
# t1 = RBX
# t2 = RCX
# t3 = RDX
# t4 = RDI
# t5 = RSI
# t6 = RBP
# t7 = RAX

# The insructions where permutated in oder to achieve higher
# performance. It is thus not easy in this version to understand
# the working principle of this algoritm. If you want to understand
# it please have a look at the 'aes_bitsliced'-project, which is
# based on the same idea but omits the permutation of the
# instructions.

    movq %r9, %rbx
    movq %r8, %rax
    rolq $16,%rbx
    rolq $16,%rax
    xorq %r9, %rbx
    xorq %r8, %rax # t0 = bit[0]^rl(bit[0],16)
    rolq $16,%rbx # t1 = rl(bit[1],16)^rl(bit[1],32)

    movq %r11, %rdx
    movq %r10, %rbp
    rolq $16,%rdx
    rolq $16,%rbp
    xorq %r11, %rdx
    xorq %r10, %rbp # t2 = bit[2]^rl(bit[2],16)   
    rolq $16,%rdx # t3 = rl(bit[3],16)^rl(bit[3],32)

    movq %r13, %rsi
    movq %r12, %rdi
    rolq $16,%rsi
    rolq $16,%rdi
    xorq %r13, %rsi
    xorq %r12, %rdi #t4 = bit[4]^rl(bit[4],16)
    rolq $16,%rsi #t5 = rl(bit[5],16)^rl(bit[5],32)     
    xorq %rdx, %r12 #rbit[4] = bit[4]^rl(bit[3],16)^rl(bit[3],32)
    xorq %rbx, %r10 #rbit[2] = bit[2]^rl(bit[1],16)^rl(bit[1],32)
    xorq %rbp,%rdx #t3 = rl(bit[3],16)^rl(bit[3],32)^bit[2]^rl(bit[2],16)
    xorq %rax,%rbx #t1 = rl(bit[1],16)^rl(bit[1],32)^bit[0]^rl(bit[0],16)
    rolq $16,%rdx #t3 = rl(bit[3],32)^rl(bit[3],48)^rl(bit[2],16)^rl(bit[2],32)
    rolq $32,%rbp #t2 = rl(bit[2],32)^rl(bit[2],48)
    xorq %rdx, %r11 #rbit[3] = rl(bit[3],32)^rl(bit[3],48)^rl(bit[2],16)^rl(bit[2],32)
    xorq %rbp, %r10 #rbit[2] = bit[2]^rl(bit[1],16)^rl(bit[1],32)^rl(bit[2],32)^rl(bit[2],48)
    rolq $32,%rax # t0 = rl(bit[0],32)^rl(bit[0],48)
    xorq 16(%rcx),%r10   #MISMATCH: "    XOR R10, [RCX+16]   "
    xorq %rax, %r8 #rbit[0] = bit[0]^rl(bit[0],32)^rl(bit[0],48)
    rolq $16,%rbx #t1 = rl(bit[1],32)^rl(bit[1],48)^rl(bit[0],16)^rl(bit[0],32)
    xorq (%rcx),%r8  #MISMATCH: "    XOR R8, [RCX]       "
    xorq %rbx, %r9 #rbit[1] = bit[1]^rl(bit[1],32)^rl(bit[1],48)^rl(bit[0],16)^rl(bit[0],32)
    movq %r15, %rax
    xorq 8(%rcx),%r9   #MISMATCH: "    XOR R9, [RCX+8]     "
    movq %r14, %rbx
    rolq $16,%rax
    rolq $16,%rbx
    xorq %r15, %rax
    xorq %r14, %rbx #t6 = bit[6]^rl(bit[6],16)
    rolq $16,%rax #t7 = rotateLeft(bit[7],16)^rotateLeft(bit[7],32)
    xorq %rsi, %r14 #bit[6] = bit[6]^rl(bit[5],16)^rl(bit[5],32)
    xorq %rax, %r8
    xorq %rdi,%rsi #t5 = rl(bit[5],16)^rl(bit[5],32)^bit[4]^rl(bit[4],16)
    xorq %rax, %r12
    rolq $32,%rdi #t4 = rl(bit[4],32)^rl(bit[4],48)
    xorq %rax, %r9
    xorq %rax, %r11
    rolq $16,%rsi #t5 = rl(bit[5],32)^rl(bit[5],48)^rl(bit[4],16)^rl(bit[4],32)
    xorq 24(%rcx),%r11  #MISMATCH: "    XOR R11, [RCX+24]    "
    xorq %rdi, %r12 #bit[4] = bit[4]^rl(bit[3],16)^rl(bit[3],32)^rl(bit[4],32)^rl(bit[4],48)
    xorq %rbx,%rax
    xorq 32(%rcx),%r12  #MISMATCH: "    XOR R12, [RCX+32]    "
    xorq %rsi, %r13 #bit[5] = bit[5]^rl(bit[5],32)^rl(bit[5],48)^rl(bit[4],16)^rl(bit[4],32)
    rolq $32,%rbx
    xorq 40(%rcx),%r13  #MISMATCH: "    XOR R13, [RCX+40]    "
    rolq $16,%rax
    xorq 48(%rcx),%rbx   #key addition
    xorq %rax, %r15
    xorq %rbx, %r14
    xorq 56(%rcx),%r15  #MISMATCH: "    XOR R15, [RCX+56]    "
ret
#MISMATCH: "mixColumnsRL16AddRk     ENDP"

#MISMATCH: "END"
