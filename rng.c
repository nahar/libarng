#include <stdio.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include "libarng.h"
//#include "aes_impl.h"
int main(int argc, char *argv[])
{
	int bflag=0,c,j;
	char *filename = NULL;
	uint8_t randnum[64],key[16];
	FILE *fp;
	if(argc<2)
	{
		fprintf (stderr, "Usage: %s [OPTIONS] filename\nOPTIONS:\n\t-b write as binary file\n",argv[0] );
		return 1;
	}
	while ((c = getopt (argc, argv, "b:")) != -1)
	{
         switch (c)
           {
           case 'b':
             bflag = 1;
             filename=optarg;
             break;
           case '?':
             if (optopt == 'c')
               fprintf (stderr, "Please specify a filename\n" );
             else if (isprint (optopt))
               fprintf (stderr, "Unknown option `-%c'.\n", optopt);
             else
               fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
             return 1;
           
           }
	   }
	if (argc==2)
	{
	   filename=argv[optind];
	}
	//fwrite(&res0,sizeof (uint8_t),16,fp);
	//	fprintf(fp,"%X",res[i][j]);
	arng_genkey(key);
	if(!arng_init(key,0,0,0))
	{
		fprintf (stderr, "Error initializing libarng\n",argv[0] );
		return 1;
	}
	arng_rand(randnum);
	fprintf (stderr, "Writing random numbers to file %s. Press Ctrl+C to abort \n",filename );
	if(fp = fopen(filename,"w"))
	{
			while(1)
			{
				arng_rand(randnum);
				if(bflag)
				{
					fwrite(randnum,sizeof (uint8_t),64,fp);
				}else
				{
					for(j=0;j<16;j++)
					{
						fprintf(fp,"%X\n",randnum[j]);
					}
				}
			}
	}
	else
	{
			fprintf (stderr, "Cant create file %s \n",filename );
			return 1;
	}
	arng_done();
	return 0;
}
