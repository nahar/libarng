# --------------------------------------------------------------------
#
# mu_swapmove_x64.asm
#
# @version 1.0 (June 2007)
#
# This file contains the implementation of the muAddRk method and the
# murk method in x64 assembler. These methods are used to transform
# 4 blocks of data in normal representation into bitslice 
# representation. The muAddRk does an additional key addition. The
# key addition and the transformation are merged for performance 
# reasons.
#
# @author Robert Könighofer <robert.koenighofer@student.tugraz.at>
#
# This code is hereby placed in the public domain.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --------------------------------------------------------------------

.global muAddRk #MISMATCH: "PUBLIC  muAddRk"
.global murk   #MISMATCH: "PUBLIC  murk"
# --------------------------------------------------------------------
#
# The swapmove-function replaces the bits in b masked by m with
# the bits in a masked by m<<n. It was taken from
# 'An Implementation of Bitsliced DES on the Pentium MMX Processor'
# by Lauren May, Lyta Penna and Andrew Clark
# This macro applies the swapmove funkctionality twice. The bits 
# in b1 masked by m are swapped with the bits in a1 masked by m<<n
# and the bits in b2 masked by m are swapped with the bits in a2 
# masked by m<<n.
# This merging is done for a higher performance.
# RSI and RDI are used as temporary registers.
.macro doubleSwapmove a1, b1, a2, b2, n, m # MISMATCH: "doubleSwapmove MACRO a1, b1, a2, b2, n, m"
    movq \a1, %rsi
    movq \a2, %rdi
    shrq \n, %rsi
    shrq \n, %rdi
    xorq \b1, %rsi
    xorq \b2, %rdi
    andq \m, %rsi
    andq \m, %rdi
    xorq %rsi, \b1
    xorq %rdi, \b2
    shlq \n, %rsi
    shlq \n, %rdi
    xorq %rsi, \a1
    xorq %rdi, \a2
.endm #MISMATCH: "ENDM"

.text #MISMATCH: "_TEXT   SEGMENT"
# ml64.exe does not allow the use of 64 bit constants as
# immediate values, we therefor define constants:
#MISMATCH: "    mask1       QWORD   5555555555555555h"
#MISMATCH: "    mask2       QWORD   3333333333333333h"
#MISMATCH: "    mask3       QWORD   0f0f0f0f0f0f0f0fh"
#MISMATCH: "    mask4       QWORD   00ff00ff00ff00ffh"
#MISMATCH: "    mask5       QWORD   0000ffff0000ffffh"
#MISMATCH: "    mask6       QWORD   00000000ffffffffh"
mask1:
       .octa   0x5555555555555555
mask2:
       .octa   0x3333333333333333
mask3:
       .octa   0x0f0f0f0f0f0f0f0f
mask4:
       .octa   0x00ff00ff00ff00ff
mask5:
       .octa   0x0000ffff0000ffff
mask6:
       .octa   0x00000000ffffffff

# --------------------------------------------------------------------
#
# This method transforms the 4 blocks of 64 byte passed in 
# RCX in the normal representation into bitslice 
# representation and writes the result into the registers R8 to %r15.
# Also the round key for the key addition of round 0 is added.  The
# address of the round key is read from the stack.
# Each of the resulting registers contains one bit. 
# The elements of the byte sequence are interpreted as elements of
# a matrix of dimension 4x4, where the elements are bytes, as defined
# in the AES algorithm. The elements of this matrix are mapped
# into the bitslice representation in the following way:
#
# register R[8+i]:
# row   00000000 00000000 11111111 11111111 22222222 22222222 33333333 33333333
# col   00001111 22223333 00001111 22223333 00001111 22223333 00001111 22223333 
# block 01230123 01230123 01230123 01230123 01230123 01230123 01230123 01230123 
# bit   iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii
#
# In this version, some calls of doubleSwapmove are inserted and
# merged with some other code for performance reasons. The code will
# therefor not be very easy to read. Have a look at the project
# 'aes_bitsliced', where the idea is the same, but the code is much
# easier to understand.
muAddRk: #MISMATCH: "muAddRk PROC"

#first doubleSwapmove merged with loading the bytes from the
#memory:
    movq 24(%rcx),%rsi #RSI = pointer to block 3
    movq 8(%rsi),%r12  #MISMATCH: "    MOV %r12,[RSI+8]    "
    movq %r12, %rax
    movq (%rsi),%r8  #MISMATCH: "    MOV %r8,[RSI]       "
    shrq $8,%rax
    movq 16(%rcx),%rdi #RDI = pointer to block 2
    xorq %r8, %rax
    movq 8(%rdi),%r13  #MISMATCH: "    MOV %r13,[RDI+8]    "
    andq (mask4), %rax
    movq (%rdi),%r9  #MISMATCH: "    MOV %r9,[RDI]       "
    movq %r13, %rdx
    movq (%rcx),%rbx   #RBX = pointer to block 0
    xorq %rax, %r8
    movq 8(%rcx),%rbp  #RBP = pointer to block 1
    shrq $8,%rdx
    movq (%rbp),%r10  #MISMATCH: "    MOV %r10,[RBP]      "
    shlq $8,%rax
    xorq %r9, %rdx
    movq (%rbx),%r11  #MISMATCH: "    MOV %r11,[RBX]      "
    andq (mask4), %rdx
    movq 8(%rbp),%r14  #MISMATCH: "    MOV %r14,[RBP+8]    "
    xorq %rdx, %r9
    xorq %rax, %r12
    shlq $8,%rdx
    movq 8(%rbx),%r15  #MISMATCH: "    MOV %r15,[RBX+8]    "
    xorq %rdx, %r13

doubleSwapmove %r8, %r12,%r9, %r13, $16, (mask5)  #MISMATCH: "    doubleSwapmove %r8,%r12,%r9,%r13,16,mask5"
doubleSwapmove %r12,%r8,%r13,%r9,$32,(mask6)  #MISMATCH: "    doubleSwapmove %r12,%r8,%r13,%r9,32,mask6"

doubleSwapmove %r14,%r10,%r15,%r11,$8,(mask4)  #MISMATCH: "    doubleSwapmove %r14,%r10,%r15,%r11,8,mask4"
doubleSwapmove %r10,%r14,%r11,%r15,$16,(mask5)  #MISMATCH: "    doubleSwapmove %r10,%r14,%r11,%r15,16,mask5"
doubleSwapmove %r14,%r10,%r15,%r11,$32,(mask6)   #MISMATCH: "    doubleSwapmove %r14,%r10,%r15,%r11,32,mask6"


doubleSwapmove %r14, %r15,%r12, %r13, $1, (mask1)  #MISMATCH: "    doubleSwapmove %r14, %r15,%r12, %r13, 1, mask1"
doubleSwapmove %r10, %r11,%r8, %r9, $1, (mask1) #MISMATCH: "    doubleSwapmove %r10, %r11,%r8, %r9, 1, mask1"

doubleSwapmove %r13, %r15,%r12, %r14, $2, (mask2)  #MISMATCH: "    doubleSwapmove %r13, %r15,%r12, %r14, 2, mask2"
doubleSwapmove %r9, %r11,%r8, %r10, $2, (mask2)  #MISMATCH: "    doubleSwapmove %r9, %r11,%r8, %r10, 2, mask2"

doubleSwapmove %r11, %r15,%r10, %r14, $4, (mask3)  #MISMATCH: "    doubleSwapmove %r11, %r15,%r10, %r14, 4, mask3"

#last doubleSwapmove merged with the key addition:

    movq 8(%rsp),%rax   #RAX contains a pointer to round key 0

    xorq 56(%rax),%r15  #MISMATCH: "    XOR %r15, [RAX+56]"
    movq %r9, %rcx
    movq %r8, %rdx
    xorq 48(%rax),%r14  #MISMATCH: "    XOR %r14, [RAX+48]"
    shrq $4,%rcx
    shrq $4,%rdx
    xorq 24(%rax),%r11  #MISMATCH: "    XOR %r11, [RAX+24]"
    xorq %r13, %rcx
    xorq %r12, %rdx
    xorq 16(%rax),%r10  #MISMATCH: "    XOR %r10, [RAX+16]"
    andq (mask3), %rcx
    andq (mask3), %rdx
    xorq %rcx, %r13
    xorq %rdx, %r12
    xorq 40(%rax),%r13  #MISMATCH: "    XOR %r13, [RAX+40]"
    shlq $4,%rcx
    shlq $4,%rdx
    xorq 32(%rax),%r12  #MISMATCH: "    XOR %r12, [RAX+32]"
    xorq %rcx, %r9
    xorq 8(%rax),%r9  #MISMATCH: "    XOR %r9, [RAX+8]"
    xorq %rdx, %r8
    xorq (%rax),%r8  #MISMATCH: "    XOR %r8, [RAX]"
ret
#MISMATCH: "muAddRk ENDP"

# --------------------------------------------------------------------
#
# This method transforms the 4 blocks of 64 byte passed in 
# RCX (the first argument as uint8_t**) from the normal representation 
# into bitslice  representation and writes the result into RDX (the 
# second argument as uint64_t*). Each
# element of the resulting array contains one bit. 
# The elements of the byte sequence are interpreted as elements of
# a matrix of dimension 4x4, where the elements are bytes, as defined
# in the AES algorithm. The elements of this matrix are mapped
# into the bitslice representation in the following way:

# target[i]:
# row   00000000 00000000 11111111 11111111 22222222 22222222 33333333 33333333
# col   00001111 22223333 00001111 22223333 00001111 22223333 00001111 22223333 
# block 01230123 01230123 01230123 01230123 01230123 01230123 01230123 01230123 
# bit   iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii iiiiiiii
#
# The working principle is the same as within muAddRk, but no xor
# interconnection with the round key is done, and the results are
# written to the address RDX.
murk:  #MISMATCH: "murk    PROC"
    pushq %r15  #MISMATCH: "    PUSH %r15"
    pushq %r14  #MISMATCH: "    PUSH %r14"
    pushq %r13  #MISMATCH: "    PUSH %r13"
    pushq %r12  #MISMATCH: "    PUSH %r12"
    pushq %r11  #MISMATCH: "    PUSH %r11"
    pushq %r10  #MISMATCH: "    PUSH %r10"
    pushq %r9  #MISMATCH: "    PUSH %r9"
    pushq %r8  #MISMATCH: "    PUSH %r8"
    pushq %rax
    pushq %rbx
    pushq %rbp
    pushq %rdi
    pushq %rsi
    pushq %rdx


    movq 24(%rcx),%rsi #RSI = pointer to block 3
    movq 8(%rsi),%r12  #MISMATCH: "    MOV %r12,[RSI+8]    "
    movq %r12, %rax
    movq (%rsi),%r8  #MISMATCH: "    MOV %r8,[RSI]       "
    shrq $8,%rax
    movq 16(%rcx),%rdi #RDI = pointer to block 2
    xorq %r8, %rax
    movq 8(%rdi),%r13  #MISMATCH: "    MOV %r13,[RDI+8]    "
    andq (mask4), %rax
    movq (%rdi),%r9  #MISMATCH: "    MOV %r9,[RDI]       "
    movq %r13, %rdx
    movq (%rcx),%rbx   #RBX = pointer to block 0
    xorq %rax, %r8
    movq 8(%rcx),%rbp  #RBP = pointer to block 1
    shrq $8,%rdx
    movq (%rbp),%r10  #MISMATCH: "    MOV %r10,[RBP]      "
    shlq $8,%rax
    xorq %r9, %rdx
    movq (%rbx),%r11  #MISMATCH: "    MOV %r11,[RBX]      "
    andq (mask4), %rdx
    movq 8(%rbp),%r14  #MISMATCH: "    MOV %r14,[RBP+8]    "
    xorq %rdx, %r9
    xorq %rax, %r12
    shlq $8,%rdx
    movq 8(%rbx),%r15  #MISMATCH: "    MOV %r15,[RBX+8]    "
    xorq %rdx, %r13

doubleSwapmove %r8,%r12,%r9,%r13,$16,(mask5)  #MISMATCH: "    doubleSwapmove %r8,%r12,%r9,%r13,16,mask5"
doubleSwapmove %r12,%r8,%r13,%r9,$32,(mask6)  #MISMATCH: "    doubleSwapmove %r12,%r8,%r13,%r9,32,mask6"

doubleSwapmove %r14,%r10,%r15,%r11,$8,(mask4)  #MISMATCH: "    doubleSwapmove %r14,%r10,%r15,%r11,8,mask4"
doubleSwapmove %r10,%r14,%r11,%r15,$16,(mask5)  #MISMATCH: "    doubleSwapmove %r10,%r14,%r11,%r15,16,mask5"
doubleSwapmove %r14,%r10,%r15,%r11,$32,(mask6)  #MISMATCH: "    doubleSwapmove %r14,%r10,%r15,%r11,32,mask6"


doubleSwapmove %r14, %r15,%r12, %r13, $1, (mask1)  #MISMATCH: "    doubleSwapmove %r14, %r15,%r12, %r13, 1, mask1"
doubleSwapmove %r10, %r11,%r8, %r9, $1, (mask1)  #MISMATCH: "    doubleSwapmove %r10, %r11,%r8, %r9, 1, mask1"

doubleSwapmove %r13, %r15,%r12, %r14, $2, (mask2)  #MISMATCH: "    doubleSwapmove %r13, %r15,%r12, %r14, 2, mask2"
doubleSwapmove %r9, %r11,%r8, %r10, $2, (mask2)  #MISMATCH: "    doubleSwapmove %r9, %r11,%r8, %r10, 2, mask2"

doubleSwapmove %r11, %r15,%r10, %r14, $4, (mask3)  #MISMATCH: "    doubleSwapmove %r11, %r15,%r10, %r14, 4, mask3"
doubleSwapmove %r9, %r13,%r8, %r12, $4, (mask3)  #MISMATCH: "    doubleSwapmove %r9, %r13,%r8, %r12, 4, mask3"

    popq %rdx
    movq %r15,56(%rdx)  #MISMATCH: "    MOV [RDX+56], %r15"
    movq %r14,48(%rdx)  #MISMATCH: "    MOV [RDX+48], %r14"
    movq %r13,40(%rdx)  #MISMATCH: "    MOV [RDX+40], %r13"
    movq %r12,32(%rdx)  #MISMATCH: "    MOV [RDX+32], %r12"
    movq %r11,24(%rdx)  #MISMATCH: "    MOV [RDX+24], %r11"
    movq %r10,16(%rdx)  #MISMATCH: "    MOV [RDX+16], %r10"
    movq %r9,8(%rdx)  #MISMATCH: "    MOV [RDX+8], %r9"
    movq %r8,(%rdx)  #MISMATCH: "    MOV [RDX], %r8"
    popq %rsi
    popq %rdi
    popq %rbp
    popq %rbx
    popq %rax
    popq %r8  #MISMATCH: "    POP %r8"
    popq %r9  #MISMATCH: "    POP %r9"
    popq %r10  #MISMATCH: "    POP %r10"
    popq %r11 #MISMATCH: "    POP %r11"
    popq %r12 #MISMATCH: "    POP %r12"
    popq %r13 #MISMATCH: "    POP %r13"
    popq %r14 #MISMATCH: "    POP %r14"
    popq %r15 #MISMATCH: "    POP %r15"
	ret
#MISMATCH: "murk    ENDP"

#MISMATCH: "END"
