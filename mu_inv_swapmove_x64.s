# --------------------------------------------------------------------
#
# mu_inv_swapmove_x64.asm
#
# @version 1.0 (June 2007)
#
# This file contains the implementation of the muInv method 
# in x64 assembler. This methods is used to transform
# 4 blocks of data in bitslice representation back into the normal 
# representation.
#
# @author Robert Könighofer <robert.koenighofer@student.tugraz.at>
#
# This code is hereby placed in the public domain.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --------------------------------------------------------------------

.global muInv  #MISMATCH: "PUBLIC  muInv"
#.global mask1
#.global mask2
#.global mask3 
#.global mask4 
#.global mask5 
#.global mask6

# --------------------------------------------------------------------
#
# The swapmove-function replaces the bits in b masked by m with
# the bits in a masked by m<<n. It was taken from
# 'An Implementation of Bitsliced DES on the Pentium MMX Processor'
# by Lauren May, Lyta Penna and Andrew Clark
# This macro applies the swapmove funkctionality twice. The bits 
# in b1 masked by m are swapped with the bits in a1 masked by m<<n
# and the bits in b2 masked by m are swapped with the bits in a2 
# masked by m<<n.
# This merging is done for a higher performance.
# RSI and RDI are used as temporary registers.
.macro doubleSwapmove a1, b1, a2, b2, n, m  #MISMATCH: "doubleSwapmove MACRO a1, b1, a2, b2, n, m"
    movq \a1, %rax
    movq \a2, %rdx
    shrq \n, %rax
    shrq \n, %rdx
    xorq \b1, %rax
    xorq \b2, %rdx
    andq \m, %rax
    andq \m, %rdx
    xorq %rax, \b1
    xorq %rdx, \b2
    shlq \n, %rax
    shlq \n, %rdx
    xorq %rax, \a1
    xorq %rdx, \a2
.endm   #MISMATCH: "ENDM"

.text#MISMATCH: "_TEXT   SEGMENT"


mask1:
       .octa   0x5555555555555555
mask2:
       .octa   0x3333333333333333
mask3:
       .octa   0x0f0f0f0f0f0f0f0f
mask4:
       .octa   0x00ff00ff00ff00ff
mask5:
       .octa   0x0000ffff0000ffff
mask6:
       .octa   0x00000000ffffffff

# --------------------------------------------------------------------  
#  
# This method transforms the result in bitslice representation back
# into the normal representation. It expects the result to be available
# in the registers R8 to R15. The corresponding 4 blocks are written
# into the address in RCX (the first passed parameter) in normal 
# representation. This function is the inverse function to mu. It
# therefor does the same operations as mu, but in reverse order.  
# Refer to mu for more details.
muInv: #MISMATCH: "muInv   PROC"

doubleSwapmove %r8, %r12,%r9, %r13, $4, (mask3)   #MISMATCH: "    doubleSwapmove R8, R12,R9, R13, 4, mask3"
 

doubleSwapmove %r10, %r14,%r11, %r15, $4, (mask3)  #MISMATCH: "    doubleSwapmove R10, R14,R11, R15, 4, mask3"

doubleSwapmove %r8,  %r10,%r9, %r11, $2,(mask2)  #MISMATCH: "    doubleSwapmove R8, R10,R9, R11, 2, mask2"
doubleSwapmove %r12, %r14,%r13, %r15, $2,(mask2) #MISMATCH: "    doubleSwapmove R12, R14,R13, R15, 2, mask2"

doubleSwapmove %r8, %r9,%r10, %r11, $1, (mask1)  #MISMATCH: "    doubleSwapmove R8, R9,R10, R11, 1, mask1"
doubleSwapmove %r12, %r13,%r14, %r15, $1,(mask1) #MISMATCH: "    doubleSwapmove R12, R13,R14, R15, 1, mask1"

doubleSwapmove %r15,%r11,%r14,%r10,$32,(mask6)  #MISMATCH: "    doubleSwapmove R15,R11,R14,R10,32,mask6"
doubleSwapmove %r11,%r15,%r10,%r14,$16,(mask5)  #MISMATCH: "    doubleSwapmove R11,R15,R10,R14,16,mask5"
doubleSwapmove %r15,%r11,%r14,%r10,$8,(mask4)  #MISMATCH: "    doubleSwapmove R15,R11,R14,R10,8,mask4"
doubleSwapmove %r13,%r9,%r12,%r8,$32,(mask6)  #MISMATCH: "    doubleSwapmove R13,R9,R12,R8,32,mask6"
doubleSwapmove %r9,%r13,%r8,%r12,$16,(mask5)  #MISMATCH: "    doubleSwapmove R9,R13,R8,R12,16,mask5"

# storing the results is merged with the last call of
# doubleSwapmove for performance reasons:    
    movq (%rcx),%rbx   #RBX = pointer to block 0   
    movq %r13, %rax
    movq 8(%rcx),%rbp  #RBP = pointer to block 1
    movq %r12, %rdx
    movq 16(%rcx),%rdi #RDI = pointer to block 2
    shrq $8,%rax
    movq 24(%rcx),%rsi #RSI = pointer to block 3 
    shrq $8,%rdx
	movq %r10,(%rbp)#MISMATCH: "    MOV [RBP],R10      "
    xorq %r9, %rax
    xorq %r8, %rdx
    movq %r15,8(%rbx)   #MISMATCH: "    MOV [RBX+8],R15    "
    andq (mask4), %rax
    andq (mask4), %rdx
    movq %r14,8(%rbp)   #MISMATCH: "    MOV [RBP+8],R14    "
    xorq %rax, %r9
    xorq %rdx, %r8
    movq %r11,(%rbx)   #MISMATCH: "    MOV [RBX],R11      "
    shlq $8,%rax
    movq %r9,(%rdi)   #MISMATCH: "    MOV [RDI],R9       "
    shlq $8,%rdx
    movq %r8,(%rsi)   #MISMATCH: "    MOV [RSI],R8       "
    xorq %rax, %r13
    movq %r13,8(%rdi)   #MISMATCH: "    MOV [RDI+8],R13    "
    xorq %rdx, %r12
    movq %r12,8(%rsi)   #MISMATCH: "    MOV [RSI+8],R12    "

ret
#MISMATCH: "muInv   ENDP"

#MISMATCH: "END"

