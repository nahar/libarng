# --------------------------------------------------------------------
#
# sbox_canright_x64.asm
#
# @version 1.0 (June 2007)
#
# This file contains the implementation of the subBytes
# step of the AES. subBytes does the transformation on the
# AES state in R15 to R8. subBytes_rk expects an array of
# 8 variables to be passed in RCX and transforms these
# variables.
# This implementation of the S-Box transformation is based on:
# 'A Very Compact Rijndael S-box' by D. Canright. His ideas
# aiming on hardware implementations were taken as base to
# form a bitslice implementation of the sbox transformation.
#
# @author Robert Könighofer <robert.koenighofer@student.tugraz.at>
#
# This code is hereby placed in the public domain.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --------------------------------------------------------------------

.global subBytes  #MISMATCH: "PUBLIC  subBytes"
.global subBytesRk  #MISMATCH: "PUBLIC  subBytesRk"
# --------------------------------------------------------------------
#
# This macro does the sbox transformation of the registers x7 to x0.
# The final inversion of the bits 0, 1, 5 and 6 is skipped
# to make the implementation faster. This is undone by inverting 
# the according bits of the round keys.
#
# Don't try to understand this implementation of the SBOX Macro.
# If you intend to do so, refer to the project 'aes_bitsliced'. The
# sbox implementation is done with the same algorithm, but this version
# has some performance optimizations, which make the code unreadable.
# (all macros are inserted, instructions are permutated and temporary
# registers are sometimes renamed to save a few moves which is 
# possible because of the reordering of the instruction) 
.macro SBOX x7, x6, x5, x4, x3, x2, x1, x0, t6, t5, t4, t3, t2, t1, t0  #MISMATCH: "SBOX MACRO x7, x6, x5, x4, x3, x2, x1, x0, t6, t5, t4, t3, t2, t1, t0 "
    movq	\x1,\t0
	xorq	\x6,\x5
	xorq	\x7,\t0
	xorq	\x0,\x5
	xorq	\x1,\x6
	xorq	\x5,\x7
	xorq	\x3,\x6
	xorq	\x5,\x1
	xorq	\x0,\x6
	xorq	\x4,\x3
	xorq	\x2,\x6
	xorq	\x5,\x4
	xorq	\t0,\x2
	xorq	\x0,\x3
	xorq	\x5,\x2
	xorq	\t0,\x3
	movq	\x4,\t1
	movq	\x2,\t2
	xorq	\x7,\t1
	xorq	\x1,\t2
	movq	\t1,-16(%rsp)
	movq	\t1,\t4
	movq	\x0,\t5
	movq	\t2,-8(%rsp)
	movq	\x3,\t6
	xorq	\x6,\t5
	xorq	\x5,\t6
	movq	\t5,-32(%rsp)
	xorq	\t2,\t4
	andq	\t5,\t1
	movq	\t6,-24(%rsp)
	andq	\t6,\t2
	movq	\t4,-40(%rsp)
	xorq	\t6,\t5
	xorq	\t1,\t2
	andq	\t5,\t4
	movq	\t5,-48(%rsp)
	movq	\x2,\t6
	xorq	\t4,\t1
	andq	\x3,\t6
	movq	\t1,-56(%rsp)
	movq	\x3,\t0
	movq	\x2,\t1
	xorq	\x0,\t0
	xorq	\x4,\t1
	movq	\x4,\t5
	movq	\t1,-64(%rsp)
	movq	\x7,\t3
	andq	\t0,\t1
	andq	\x6,\t3
	movq	\t0,-72(%rsp)
	andq	\x0,\t5
	xorq	\t1,\t6
	xorq	\t1,\t5
	movq	\x6,-96(%rsp)
	movq	\x1,\t1
	movq	\x5,\t0
	xorq	\x7,\t1
	xorq	\x6,\t0
	movq	\t1,-80(%rsp)
	xorq	\t2,\t5
	movq	\x1,\t4
	movq	\x5,-104(%rsp)
	andq	\t0,\t1
	andq	\x5,\t4
	xorq	\x7,\x6
	movq	\t0,-88(%rsp)
	xorq	\t1,\t4
	xorq	\t1,\t3
	xorq	\x1,\x5
	movq	-56(%rsp),\t1
	xorq	\t2,\t3
	xorq	\x5,\t5
	xorq	\x6,\t3
	xorq	\x2,\t5
	xorq	\x6,\t1
	xorq	\x0,\t6
	xorq	\t1,\t4
	xorq	\t1,\t6
	xorq	\x5,\t4
	xorq	\x4,\t6
	movq	\t4,\t2
	xorq	\x3,\t5
	xorq	\t3,\t2
	movq	\t5,\x6
	movq	\t5,\t0
	xorq	\t6,\x6
	andq	\t3,\t0
	movq	\t2,\x5
	xorq	\t3,\t0
	andq	\x6,\x5
	xorq	\t5,\t0
	xorq	\t6,\x5
	movq	\t6,\t1
	xorq	\t4,\x5
	andq	\t4,\t1
	xorq	\x5,\t0
	xorq	\x5,\t1
	andq	\t0,\t6
	andq	\t0,\t4
	movq	-104(%rsp),\x5
	andq	\t1,\t5
	xorq	\t1,\t0
	andq	\t1,\t3
	andq	\t0,\x6
	andq	\t0,\t2
	xorq	\x6,\t6
	xorq	\t2,\t4
	xorq	\x6,\t5
	xorq	\t2,\t3
	movq	\t6,\t1
	movq	-96(%rsp),\x6
	andq	\t4,\x3
	xorq	\t4,\t1
	movq	-24(%rsp),\t2
	andq	\t4,\x2
	andq	\t1,-8(%rsp)
	andq	\t6,\x5
	xorq	\t3,\t4
	andq	\t6,\x1
	andq	\t4,-64(%rsp)
	andq	\t3,\x0
	xorq	\t5,\t6
	andq	-72(%rsp),\t4
	andq	\t3,\x4
	andq	\t6,-80(%rsp)
	andq	\t5,\x7
	xorq	\t5,\t3
	andq	-88(%rsp),\t6
	andq	\t5,\x6
	andq	\t1,\t2
	andq	\t3,-16(%rsp)
	xorq	\t6,\x5
	movq	-64(%rsp),\t5
	xorq	\t6,\x6
	movq	\t1,\t6
	xorq	\t4,\x3
	xorq	\t3,\t1
	andq	-32(%rsp),\t3
	xorq	\t4,\x0
	xorq	\t5,\x2
	andq	\t1,-40(%rsp)
	xorq	\t3,\t2
	andq	-48(%rsp),\t1
	xorq	\t2,\x0
	movq	-8(%rsp),\t0
	xorq	\t3,\t1
	xorq	\t5,\x4
	movq	-80(%rsp),\t3
	xorq	\t2,\x6
	xorq	\t1,\x3
	xorq	\t1,\x5
	movq	\x0,\t6
	movq	-16(%rsp),\t1
	xorq	\x6,\x0
	xorq	\t3,\x1
	xorq	\t1,\t0
	xorq	\t3,\x7
	xorq	-40(%rsp),\t1
	xorq	\t0,\x4
	xorq	\t0,\x7
	xorq	\t1,\x1
	xorq	\t1,\x2
	movq	\x1,\t2
	movq	\x4,\t1
	xorq	\x6,\x1
	movq	\x5,\x4
	movq	\x2,\x6
	xorq	\x5,\x1
	xorq	\x3,\x6
	movq	\x7,\t0
	xorq	\x6,\x4
	movq	\x0,\x3
	movq	\x5,\x7
	xorq	\x4,\x3
	xorq	\x2,\x7
	movq	\t6,\x5
	movq	\x7,\x2
	xorq	\t0,\x5
	xorq	\t1,\x2
	xorq	\t2,\x0
	xorq	\x5,\x2
.endm #MISMATCH: "ENDM"

.text  #MISMATCH: "_TEXT   SEGMENT"

# --------------------------------------------------------------------
#
# This method implements the SubBytes step of the AES in a bitsliced 
# way. The method updates the state, which has to be in bitslice 
# representation in the registers R8 to R15. The implementation is
# based on:
# 'A Very Compact Rijndael S-box' by D. Canright.
# The final inversion of the bits 0, 1, 5 and 6 is skipped
# to make the implementation faster. This is undone by inverting 
# the according bits of the round keys.
subBytes:  #MISMATCH: "subBytes        PROC"
SBOX %r15, %r14, %r13, %r12, %r11, %r10, %r9, %r8, %rax, %rbx, %rcx, %rdx, %rbp, %rsi, %rdi  #MISMATCH: "    SBOX R15, R14, R13, R12, R11, R10, R9, R8, RAX, RBX, RCX, RDX, RBP, RSI, RDI"
ret
#MISMATCH: "subBytes        ENDP"

# --------------------------------------------------------------------
#
# This method implements the SubBytes step  of the AES in a bitsliced 
# way. The method transforms the data passed in RCX (the first argument
# as uint64_t* value) , which has to be in
# bitslice representation. It is used for the calculation of the
# round keys only and uses the subBytes method. 
subBytesRk: #MISMATCH: "subBytesRk      PROC"
    pushq %rbp
    pushq %rbx
    pushq %r12  #MISMATCH: "    PUSH R12"
    pushq %r13  #MISMATCH: "    PUSH R13"
    pushq %r14  #MISMATCH: "    PUSH R14"
    pushq %r15  #MISMATCH: "    PUSH R15"
    pushq %rsi
    pushq %rdi
    pushq %rcx
    movq 56(%rcx), %r15  #MISMATCH: "    MOV R15, [RCX+56]"
    movq 48(%rcx), %r14  #MISMATCH: "    MOV R14, [RCX+48]"
    movq 40(%rcx), %r13  #MISMATCH: "    MOV R13, [RCX+40]"
    movq 32(%rcx), %r12  #MISMATCH: "    MOV R12, [RCX+32]"
    movq 24(%rcx), %r11  #MISMATCH: "    MOV R11, [RCX+24]"
    movq 16(%rcx), %r10  #MISMATCH: "    MOV R10, [RCX+16]"
    movq 8(%rcx), %r9  #MISMATCH: "    MOV R9, [RCX+8]"
    movq (%rcx), %r8  #MISMATCH: "    MOV R8, [RCX]"
    call subBytes
# we can not skip the final inversion of the bits
# of the bits 0, 1, 5 and 6 here:
    notq %r8  #MISMATCH: "    NOT R8"
    notq %r9  #MISMATCH: "    NOT R9   "
    notq %r13 #MISMATCH: "    NOT R13"
    notq %r14  #MISMATCH: "    NOT R14 "

    popq %rcx
    movq %r15, 56(%rcx)  #MISMATCH: "    MOV [RCX+56], R15"
    movq %r14, 48(%rcx)  #MISMATCH: "    MOV [RCX+48], R14"
    movq %r13, 40(%rcx)  #MISMATCH: "    MOV [RCX+40], R13"
    movq %r12, 32(%rcx)  #MISMATCH: "    MOV [RCX+32], R12"
    movq %r11, 24(%rcx)  #MISMATCH: "    MOV [RCX+24], R11"
    movq %r10, 16(%rcx)  #MISMATCH: "    MOV [RCX+16], R10"
    movq %r9, 8(%rcx)  #MISMATCH: "    MOV [RCX+8], R9"
    movq %r8, (%rcx)  #MISMATCH: "    MOV [RCX], R8"
    popq %rdi
    popq %rsi
    popq %r15  #MISMATCH: "    POP R15"
    popq %r14  #MISMATCH: "    POP R14"
    popq %r13  #MISMATCH: "    POP R13"
    popq %r12  #MISMATCH: "    POP R12"
    popq %rbx
    popq %rbp
ret
#MISMATCH: "subBytesRk      ENDP"

#MISMATCH: "END"
