# --------------------------------------------------------------------
#
# encrypt_bitsliced_x64.asm
#
# @version 1.0 (June 2007)
#
# This file contains the implementation of the encryptBitsliced
# method in x64 assembler. This method does the encrytion of 4 blocks 
# of data in a bitsliced manner.
#
# @author Robert Könighofer <robert.koenighofer@student.tugraz.at>
#
# This code is hereby placed in the public domain.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --------------------------------------------------------------------

.global encryptBitsliced # MISMATCH: "PUBLIC  encryptBitsliced"
.extern subBytes:PROC
.extern mixColumnsRL16AddRk:PROC
.extern shiftRowsAddRk:PROC
.extern shiftRowsRR16:PROC
.extern muAddRk:PROC
.extern muInv:PROC

.text #MISMATCH: "_TEXT   SEGMENT"

# --------------------------------------------------------------------
#
# This method encrypts 4 blocks of data passed in register RCX 
# (in the first argument as uint8_t**) with the
# round keys in bitslice representation passed in R8 (in the third
# argument as uint64_t[11][8]). The result
# is written into RDX (to the second argument as uint8_t**). 
# The implementation is doen in x64 assembler.
#MISMATCH: "encryptBitsliced        PROC"
encryptBitsliced:
    pushq %r12       #MISMATCH: "    PUSH R12"
    pushq %r13       #MISMATCH: "    PUSH R13"
    pushq %r14       #MISMATCH: "    PUSH R14"
    pushq %r15       #MISMATCH: "    PUSH R15"
    pushq %rbp
    pushq %rsi
    pushq %rdi
    pushq %rbx
    pushq %rdx  # the target address passed as second argument
    pushq %r8        #MISMATCH: "    PUSH R8     "

# Transformation into the bitslice domain:    
# RCX contains a pointer to the data (the first argument)
# The address to the round key is read from the stack
    call muAddRk

# round 1:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key
    addq $64,%rcx             #RCX = round_key[1]
    call mixColumnsRL16AddRk

# round 2:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key
    addq $128,%rcx            #RCX = round_key[2]
    call mixColumnsRL16AddRk

# round 3:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key
    addq $192,%rcx            #RCX = round_key[3]
    call mixColumnsRL16AddRk

# round 4:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key
    addq $256,%rcx            #RCX = round_key[4]
    call mixColumnsRL16AddRk

# round 5:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key   
    addq $320,%rcx            #RCX = round_key[5]
    call mixColumnsRL16AddRk

# round 6:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key  
    addq $384,%rcx            #RCX = round_key[6]
    call mixColumnsRL16AddRk

# round 7:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key  
    addq $448,%rcx            #RCX = round_key[7]
    call mixColumnsRL16AddRk

# round 8:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key  
    addq $512,%rcx            #RCX = round_key[8]
    call mixColumnsRL16AddRk

# round 9:
    call subBytes
    call shiftRowsRR16
    movq (%rsp),%rcx          #RCX = round_key  
    addq $576,%rcx            #RCX = round_key[9]
    call mixColumnsRL16AddRk

# round 10:
    call subBytes
    popq %rcx                 #RCX = round_key
    call shiftRowsAddRk

    popq %rcx                 #RCX = target address

# The transformation back into the normal domain:
# RCX contains the target address
    call muInv
    popq %rbx
    popq %rdi
    popq %rsi
    popq %rbp
    popq %r15    #MISMATCH: "    POP R15"
    popq %r14    #MISMATCH: "    POP R14"
    popq %r13    #MISMATCH: "    POP R13"
    popq %r12    #MISMATCH: "    POP R12"
ret
#MISMATCH: "encryptBitsliced        ENDP"

#
