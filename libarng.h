/**
 * @file libarng.h
 * @version 1.0
 * @date 22 Jan 2012
 * @brief libarng - include file
 * libarng 1.0
 * Fast Random Nmuber generator based on bitsliced AES.
 * Supports seeding keys form /dev/random
 */

#ifndef AES_RNG_H_INCLUDED
#define AES_RNG_H_INCLUDED

#ifndef uint8_t
typedef unsigned char uint8_t;
#endif


/** \brief Generates 128bit random key
 *
 * \param key[16] buffer reciving 128bit key
 * \return void
 *
 */
void arng_genkey(uint8_t key[16]);

/** \brief Seeds the RNG key
 *
 * \param k1 uint8_t* Key for block1
 * \param k2 uint8_t* Key for block2, uses k1 if 0
 * \param k3 uint8_t* Key for block3, uses k1 if 0
 * \param k4 uint8_t* Key for block4, uses k1 if 0
 * \return 0 if success. non zero if ther is a error
 *
 */
int arng_init(const uint8_t* k1,const uint8_t* k2,const uint8_t* k3,const uint8_t* k4);

/** \brief Genereates random 512bits.
 *
 * \param rnd[64] uint8_t buffer to store the 512 bit number.
 * \return void
 *
 */
void arng_rand(uint8_t rnd[64]);


/** \brief To be called once to free resources
 *
  * \return void
 *
 */
void arng_done(void);


#endif // AES_RNG_H_INCLUDED
