#include <stdio.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include<unistd.h>
#include<stdlib.h>
#include "libarng.h"
#include "aes_impl.h"


uint64_t reg_save[15];

uint8_t* _arng_key1=0;
uint8_t* _arng_key2=0;
uint8_t* _arng_key3=0;
uint8_t* _arng_key4=0;
uint8_t* _arng_msg1=0;
uint8_t* _arng_msg2=0;
uint8_t* _arng_msg3=0;
uint8_t* _arng_msg4=0;
uint8_t* _arng_keys[4];
uint8_t* _arng_msgs[4];
uint8_t* _arng_res[4];
uint64_t counter=-1;

uint64_t rk_bs[11][8];


void arng_genkey(uint8_t key[16])
{
    int randomData = open("/dev/urandom", O_RDONLY);
    read(randomData,key, 16);
	close(randomData);

}
int arng_init(const uint8_t* k1,const uint8_t* k2,const uint8_t* k3,const uint8_t* k4)
{
    int randomData;
    if(k1)
    {
        if(_arng_key1=malloc(16))
        {
                memcpy(_arng_key1,k1,16);
                _arng_keys[0]=_arng_key1;
        }else
        {
            return 2;
        }

    }else
    {
        return 1;
    }
    if(k2)
    {
        if(_arng_key2=malloc(16))
        {
                memcpy(_arng_key2,k2,16);
                _arng_keys[1]=_arng_key2;
        }else
        {
            return 2;
        }

    }else
    {
         _arng_keys[1]=_arng_key1;
    }
    if(k3)
    {
        if(_arng_key3=malloc(16))
        {
                memcpy(_arng_key3,k3,16);
                _arng_keys[2]=_arng_key3;
        }else
        {
            return 2;
        }

    }else
    {
        _arng_keys[2]=_arng_key1;
    }
    if(k4)
    {
        if(_arng_key4=malloc(16))
        {
                memcpy(_arng_key4,k4,16);
                _arng_keys[3]=_arng_key4;
        }else
        {
            return 2;
        }

    }else
    {
         _arng_keys[3]=_arng_key1;
    }
	if((_arng_msg1=malloc(16))==0 ||(_arng_msg2=malloc(16))==0 ||(_arng_msg3=malloc(16))==0 ||(_arng_msg4=malloc(16))==0  )
	{
	    return 2;
	}
    randomData = open("/dev/urandom", O_RDONLY);
	read(randomData,(uint64_t*)(_arng_msg1+8), sizeof (uint64_t));// hiwor of msg1=random
	close(randomData);
    *(uint64_t*)(_arng_msg2+8)=*(uint64_t*)(_arng_msg1+8);//hiword of msg2= hiwor of msg1
    *(uint64_t*)(_arng_msg3+8)=*(uint64_t*)(_arng_msg1+8);//hiword of msg3= hiwor of msg1
    *(uint64_t*)(_arng_msg4+8)=*(uint64_t*)(_arng_msg1+8);//hiword of msg3= hiwor of msg1
    *(uint64_t*)_arng_msg1=*(uint64_t*)_arng_keys[0];//loword of msg1=loword of key1
    *(uint64_t*)_arng_msg2=*(uint64_t*)_arng_keys[1];//loword of msg1=loword of key1
    *(uint64_t*)_arng_msg3=*(uint64_t*)_arng_keys[2];//loword of msg1=loword of key1
    *(uint64_t*)_arng_msg4=*(uint64_t*)_arng_keys[3];//loword of msg1=loword of key1
	_arng_msgs[0]=_arng_msg1;
	_arng_msgs[1]=_arng_msg2;
	_arng_msgs[2]=_arng_msg3;
	_arng_msgs[3]=_arng_msg4;
}

void arng_rand(uint8_t rnd[64])
{
	*(uint64_t*)(_arng_msg1+8)=(*(uint64_t*)(_arng_msg4+8))+1;//hiwordmsg1++
    *(uint64_t*)(_arng_msg2+8)=(*(uint64_t*)(_arng_msg1+8))+1;//hiwordmsg2++
    *(uint64_t*)(_arng_msg3+8)=(*(uint64_t*)(_arng_msg2+8))+1;//hiwordmsg3++
    *(uint64_t*)(_arng_msg4+8)=(*(uint64_t*)(_arng_msg3+8))+1;//hiwordmsg4++
    _arng_res[0]=rnd;
    _arng_res[1]=rnd+16;
    _arng_res[2]=rnd+32;
    _arng_res[3]=rnd+48;

	encrypt(_arng_msgs, _arng_res,rk_bs);
}

void arng_done(void)
{
    free(_arng_key1);
    free(_arng_key2);
    free(_arng_key3);
    free(_arng_key4);
    free(_arng_msg1);
    free(_arng_msg2);
    free(_arng_msg3);
    free(_arng_msg4);

}
