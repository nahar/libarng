# --------------------------------------------------------------------
#
# save_restore_state_x64.asm
#
# @version 1.0 (June 2007)
#
# This file contains a procedure to store all registers
# and to restore all registers into some fixed location
# in the memory (reg_save). This is only used as most
# of the assembler routines do not store and restore the
# registers. In order to be able to call the procedures
# from the test program and to measure their number of
# CPU ticks, save() has to be called before and
# restore() has to be called after calling the assembler
# routine to be measured.
#
# @author Robert Könighofer <robert.koenighofer@student.tugraz.at>
#
# This code is hereby placed in the public domain.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --------------------------------------------------------------------

.global save #MISMATCH: "PUBLIC  save"
.global restore #MISMATCH: "PUBLIC  restore"
.extern reg_save:PTR


.text  #MISMATCH: "_TEXT   SEGMENT"
# This method saves all registers to the memory into the array 
# reg_save. This is typically
# done before assembler code is executed, which does not restore
# the register values. This assembler code is not inteded to be called
# from outside, but this is done anyway in order to be able to make
# performance measures on these parts. 
save: #MISMATCH: "save    PROC"
    movq %r8,reg_save  #MISMATCH: "    MOV [reg_save],R8"
    movq %r9,reg_save+8  #MISMATCH: "    MOV [reg_save+8],R9"
    movq %r10,reg_save+16  #MISMATCH: "    MOV [reg_save+16],R10"
    movq %r11,reg_save+24  #MISMATCH: "    MOV [reg_save+24],R11"
    movq %r12,reg_save+32  #MISMATCH: "    MOV [reg_save+32],R12"
    movq %r13,reg_save+40  #MISMATCH: "    MOV [reg_save+40],R13"
    movq %r14,reg_save+48  #MISMATCH: "    MOV [reg_save+48],R14"
    movq %r15,reg_save+56  #MISMATCH: "    MOV [reg_save+56],R15"
    movq %rax,reg_save+64
    movq %rbx,reg_save+72
    movq %rcx,reg_save+80
    movq %rdx,reg_save+88
    movq %rdi,reg_save+96
    movq %rsi,reg_save+104
    movq %rbp,reg_save+112
ret
#MISMATCH: "save    ENDP"

# This method recovers all registers from the array 
# reg_save. This can only be done, if they were saved by the
# save method before. 
restore:  #MISMATCH: "restore PROC"
    movq reg_save+112,%rbp
    movq reg_save+104,%rsi
    movq reg_save+96,%rdi
    movq reg_save+88,%rdx
    movq reg_save+80,%rcx
    movq reg_save+72,%rbx
    movq reg_save+64,%rax
    movq reg_save+56,%r15  #MISMATCH: "    MOV R15,[reg_save+56]"
    movq reg_save+48,%r14  #MISMATCH: "    MOV R14,[reg_save+48]"
    movq reg_save+40,%r13  #MISMATCH: "    MOV R13,[reg_save+40]"
    movq reg_save+32,%r12  #MISMATCH: "    MOV R12,[reg_save+32]"
    movq reg_save+24,%r11  #MISMATCH: "    MOV R11,[reg_save+24]"
    movq reg_save+16,%r10  #MISMATCH: "    MOV R10,[reg_save+16]"
    movq reg_save+8,%r9  #MISMATCH: "    MOV R9,[reg_save+8]"
    movq reg_save,%r8  #MISMATCH: "    MOV R8,[reg_save]"
ret
#MISMATCH: "restore ENDP"

#MISMATCH: "END"
