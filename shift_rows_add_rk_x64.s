# --------------------------------------------------------------------
#
# shift_rows_add_rk_x64.asm
#
# @version 1.0 (June 2007)
#
# This file contains an implementation of the ShiftRows
# transformation of the bitsliced AES state followed 
# by the AddRoundKey operation. The two opertions are merged for
# performance reasons.
#
# @author Robert Könighofer <robert.koenighofer@student.tugraz.at>
#
# This code is hereby placed in the public domain.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --------------------------------------------------------------------

.global shiftRowsAddRk  #MISMATCH: "PUBLIC  shiftRowsAddRk"

# --------------------------------------------------------------------
#
# This macro rotates the lowest 16 bit (they correspond to
# row 3) of reg by 12 positions to the left, which is
# equivalent to rotating row 3 of the matrix by 3 
# elements to the left.
# The bits 16 to 31 correspond to row 2 and they are rotated
# by 8 positions to the left, which is equivalent to rotating
# row 2 of the matrix by 2 elements to the left.
# The bits 32 to 47 correspond to row 1 and they are rotated
# by 4 positions to the left, which is equivalent to rotating
# row 1 of the matrix by 1 element to the left.
# The bits 48 to 63 stay untouched as they correspond to row 0
# and row 0 is not rotated. On x64 platforms, the lowest 16 bit
# of each 64 bit register is available as own register. This
# is used for the rotations of the subgroups of 16 bytes inside
# the 64 bit registers.
.macro rotate reg, reg16   #MISMATCH: "rotate MACRO reg, reg16"
      rol $12,\reg16   #MISMATCH: "    ROL reg16,12"
      ror $16,\reg    #MISMATCH: "    ROR reg,16"
      rol $8,\reg16   #MISMATCH: "    ROL reg16,8"
      ror $16,\reg   #MISMATCH: "    ROR reg,16"
      rol $4,\reg16   #MISMATCH: "    ROL reg16,4"
      rol $32,\reg   #MISMATCH: "    ROL reg,32"
.endm  #MISMATCH: "ENDM"

.text  #MISMATCH: "_TEXT   SEGMENT"

# --------------------------------------------------------------------
#  
# This method does the ShiftRows step followed by the AddRoundKey step
# of the AES in a bitsliced way. This is necessary since the
# MixColumns step is not done in the last round. Therefor the 
# mixColumnsRL16AddRk method can not be executed in combination 
# with the shiftRowsRR16 as in all other rounds. The ShiftRows
# step and the AddRoundKey step are put together for performance
# reasons. The method expects the state to be in the registers R8 
# to R15 in bitslice representation and updates this state.
# A pointer to an array of all round keys is passed in RCX (the
# first argument)
shiftRowsAddRk:  #MISMATCH: "shiftRowsAddRk  PROC"
        rotate %r15,%r15w   #MISMATCH: "    rotate R15, R15W"
        xorq 696(%rcx),%r15   #MISMATCH: "    XOR R15, QWORD PTR [RCX+640+56]"
        rotate %r14,%r14w   #MISMATCH: "    rotate R14, R14W"
        xorq 688(%rcx),%r14   #MISMATCH: "    XOR R14, QWORD PTR [RCX+640+48]"
        rotate %r13,%r13w   #MISMATCH: "    rotate R13, R13W"
        xorq 680(%rcx),%r13   #MISMATCH: "    XOR R13, QWORD PTR [RCX+640+40] "
        rotate %r12,%r12w   #MISMATCH: "    rotate R12, R12W"
        xorq 672(%rcx),%r12   #MISMATCH: "    XOR R12, QWORD PTR [RCX+640+32]"
        rotate %r11,%r11w   #MISMATCH: "    rotate R11, R11W"
        xorq 664(%rcx),%r11   #MISMATCH: "    XOR R11, QWORD PTR [RCX+640+24]"
        rotate %r10,%r10w   #MISMATCH: "    rotate R10, R10W"
        xorq 656(%rcx),%r10   #MISMATCH: "    XOR R10, QWORD PTR [RCX+640+16]"
        rotate %r9,%r9w   #MISMATCH: "    rotate R9, R9W"
        xorq 648(%rcx),%r9  #MISMATCH: "    XOR R9, QWORD PTR [RCX+640+8]"
        rotate %r8,%r8w   #MISMATCH: "    rotate R8, R8W"
        xorq 640(%rcx),%r8  #MISMATCH: "    XOR R8, QWORD PTR [RCX+640]    "
ret
#MISMATCH: "shiftRowsAddRk  ENDP"

#MISMATCH: "END"
