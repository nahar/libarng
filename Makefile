all:
			gcc -m64 rng.c libarng.c encrypt_bitsliced_x64.s mix_cols_rl16_add_rk_x64.s mu_swapmove_x64.s mu_inv_swapmove_x64.s save_restore_state_x64.s shift_rows_add_rk_x64.s shift_rows_rr16_x64.s sbox_canright_x64.s aes_impl.c -o rng

rsdf:
		gcc -m64 aes_test.c encrypt_bitsliced_x64.s mix_cols_rl16_add_rk_x64.s mu_swapmove_x64.s mu_inv_swapmove_x64.s save_restore_state_x64.s shift_rows_add_rk_x64.s shift_rows_rr16_x64.s sbox_canright_x64.s aes_impl.c rijndael-alg-fst.c -o aes_test


clean:
	rm -f *.o aes_test rng
